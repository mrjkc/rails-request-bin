class Xml < ApplicationRecord
  require "rexml/document"
  require "json"

  def self.format_data(data)
    if valid_json?(data)
      JSON.parse(data)
      return data
    end
    begin
      @doc = REXML::Document.new(data)
      @out = ""
      @doc.write(@out, 1)
      return @out
    rescue
      return "Invalid Data"
    end
  end

  private
  def self.valid_json?(json)
      JSON.parse(json)
      return true
    rescue JSON::ParserError => e
      return false
  end
end
