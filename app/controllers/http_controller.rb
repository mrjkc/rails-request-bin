class HttpController < ApplicationController
  skip_before_action :verify_authenticity_token

  def http_post
    @xml_body = request.body.read
    if @xml_body.present?
      @xml = Xml.create(:xml_body => @xml_body)
    end
    @xmls = Xml.all.sort.reverse
  end

  def destroy_them_all
    Xml.destroy_all
    redirect_to root_path
  end
end
