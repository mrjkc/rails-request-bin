# Request Bin for testing HTTP calls

[demo!](https://aqueous-taiga-13925.herokuapp.com)

Required:

* Heroku Account

* Rails 5.1

* Clone to your local repository:

  ```
  $ git clone https://gitlab.com/mrjkc/rails-request-bin.git
  ```

* To run:

  ```
  $ cd rails-request-bin
  $ bundle install
  $ rails s
  ```

* Access in the browser at localhost:3000

* ALL TEST REQUESTS SHOULD BE MADE TO: http://localhost:3000/http_post

### How to deploy to Heroku:

* Create a new Heroku account.

  ```
  $ heroku login
  ```

* In Gemfile, add the pg gem to your Rails project. Change:

  ```
  gem sqlite
  ```

  to

  ```
  gem 'sqlite3', group: :development
  gem 'pg', '0.18.1', group: :production
  ```

* In Gemfile, add the rails_12factor gem::

  ```
  gem 'rails_12factor', group: :production
  ```

* In the terminal, install the gems specified in the Gemfile:

  ```
  $ bundle install
  ```

* Ensure config/database.yml is using the postgresql adapter. Change:

```
production:
  <<: \*default
  database: db/production.sqlite3
```

to

```
production:
  <<: \*default
  adapter: postgresql
  database: db/production.sqlite3
```

* Commit your changes to git:

  ```
  $ git add .
  $ git commit -m "Heroku config"
  ```

* In the terminal, create an app on Heroku:

  ```
  $ heroku create
  ```

* Push your code to Heroku:

  ```
  $ git push heroku master
  ```

* Migrate the database by running:
  ```
  $ heroku run rake db:migrate
  ```
