class CreateXmls < ActiveRecord::Migration[5.1]
  def change
    create_table :xmls do |t|

      t.string :xml_body
      t.string :xml_header
      t.string :src_url
      t.string :status
      t.string :response

      t.timestamps

    end
  end
end
