Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'http#http_post'
  post 'http_post' => 'http#http_post'
  get 'http_post' => 'http#http_post'

  match '/destroy_them_all', to: 'http#destroy_them_all', via: :delete
end
